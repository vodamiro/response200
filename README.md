Response200
============

Micro HTTP server that returns HTTP response with status code 200 and body with text "OK" (with newline character at the end).

Expected HTTP response:
```
< HTTP/1.1 200 OK
< X-Powered-By: Express
< Content-Type: text/html; charset=utf-8
< Content-Length: 3
< Connection: keep-alive
<
OK
```

## Sources

You can get source codes from GIT repository:

```
git clone https://vodamiro@bitbucket.org/vodamiro/response200.git
```

## Usage

### Run using Docker

You can run the server as Docker container, to do that, use following command:

```
PORT=3000
docker run -p $PORT:3000 -d vodamiro/response200
```

where value of `PORT` is the port number on which the service will run. Defaultly the server runs on port 3000.

### Run from sources

Or you can start server manually using following commands:


```
git clone https://vodamiro@bitbucket.org/vodamiro/response200.git
cd response200
npm install
npm start
```

Defaultly the server runs on port 3000.

## Used tools and Libraries

* [Docker](https://www.docker.com/)
* [Alpine Linux](https://alpinelinux.org/)
* [Node.js](https://nodejs.org/)
* [express](https://www.npmjs.com/package/express)
